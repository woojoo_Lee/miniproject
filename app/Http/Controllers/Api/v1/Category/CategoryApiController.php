<?php

namespace App\Http\Controllers\Api\v1\Category;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Category;
use App\Lecture;
use Illuminate\Support\Facades\Log;

class CategoryApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info("===" . __CLASS__ . " , " . __FUNCTION__ . " : start Log");

        $Categories = Category::all();

        Log::info("===" . __CLASS__ . " , " . __FUNCTION__ . " : end Log");
        return response()->json($Categories, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        Log::info("===" . __CLASS__ . " , " . __FUNCTION__ . " : start Log");

        // validator 유효성검사
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'parent_id' => 'nullable|string',

        ]);
        // validator 실패하면
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()
            ], 400);
        }

        $title = $request->input("title"); //trim
        $parentId = $request->input("parent_id") ?? null;

     
        $isTitleExist = Category::where("title", $title)->exists();
        $isParentidExist = Category::where("_id", $parentId)->exists();

        
        // title 중복검사
        if ($isTitleExist) {
            return response()->json([
                'message' => '중복된 제목입니다.'
            ], 401);
        }
        // 만약 parentId와 일치하는 카테고리ID가 없으면 실패
        if($parentId&&!$isParentidExist){
            return response()->json([
                'message' => 'parent_id와 일치하는 카테고리ID가 없습니다.'
            ], 402);
        }

        $result = $this->createCategory($title,$parentId);

        return response()->json($result, 200);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        Log::info("===" . __CLASS__ . " , " . __FUNCTION__ . " : start Log");

        $result = Lecture::where("category_id", $id)->get();

        if(count($result)==0){
            return response()->json([
                'message' => '해당하는 카테고리 ID와 일치하는 강의가 없습니다.'
            ], 402);
        }
     

        //
        return response()->json($result, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Log::info("===" . __CLASS__ . " , " . __FUNCTION__ . " : start Log");


        $result = Category::where("_id", $id)->delete();

        if(count($result)==0){
            return response()->json([
                'message' => '해당하는 카테고리 ID와 일치하는 데이터가 없습니다.'
            ], 402);
        }

        //
        return response()->json($result, 200);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
    private function createCategory($title,$parentId){

        $category = Category::create([
            'title' =>  $title,
            'parent_id' => $parentId
        ]);

        return $category;
    }
}
