<?php

namespace App\Http\Controllers\Api\v1\Lecture;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lecture;
use App\Category;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class LectureApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info("===" . __CLASS__ . " , " . __FUNCTION__ . " : start Log");

        $lectures = Lecture::all();

        Log::info("===" . __CLASS__ . " , " . __FUNCTION__ . " : end Log");
        return response()->json($lectures, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        Log::info("===" . __CLASS__ . " , " . __FUNCTION__ . " : start Log");
        //category_id  title 존재 유무 판단

        $validator = Validator::make($request->all(), [
            'category_id' => 'required|string',
            'title' => 'required|string',
            'teacher' => 'required|string',
            'lecture_image' => 'required|string',


        ]);
        // validator 실패하면
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()
            ], 400);
        }

        $categoryId = $request->input("category_id"); //trim
        $title = $request->input("title"); //trim
        $teacher = $request->input("teacher"); //trim
        $lecture_image = $request->input("lecture_image"); //trim
        $description = $request->input("description"); //trim

       
        $isCategoryidExist = Category::where("_id", $categoryId)->exists();
        $isTitleExist = Lecture::where("title", $title)->exists();

        if($isTitleExist){
            return response()->json([
                'message' => '중복된 제목입니다.'
            ], 402);
        }
        if(!$isCategoryidExist){
            return response()->json([
                'message' => '해당하는 카테고리ID값이 없습니다.'
            ], 403);
        }

        $result = $this->createLecture($categoryId,$title,$teacher,$lecture_image,$description);

        return response()->json($result, 200);
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Log::info("===" . __CLASS__ . " , " . __FUNCTION__ . " : start Log");

        $result = Lecture::where("_id", $id)->first();

        if(count($result)==0){
            return response()->json([
                'message' => '해당하는  ID와 일치하는 데이터가 없습니다.'
            ], 402);
        }
        return $result;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function image($url)
    {
        Log::info("===" . __CLASS__ . " , " . __FUNCTION__ . " : start Log");

        $isExistFile=Lecture::where("lecture_image",$url)->exists();
        if(!$isExistFile){
            return response()->json([
                'message' => '해당하는  파일이 없습니다.'
            ], 405);

        }
        $contents = Storage::get($url);
      
        
        return response($contents)->header('Content-type', 'image/png');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Log::info("===" . __CLASS__ . " , " . __FUNCTION__ . " : start Log");

        $result = Lecture::where("_id", $id)->delete();

        if(count($result)==0){
            return response()->json([
                'message' => '해당하는  ID와 일치하는 데이터가 없습니다.'
            ], 402);
        }

        return response()->json($result, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    private function createLecture($categoryId,$title,$teacher,$lecture_image,$description){
      
        $Lecture = Lecture::create([
            'category_id' =>  $categoryId,
            'title' => $title,
            'teacher' => $teacher,
            'lecture_image' => $lecture_image,
            'description' => $description
        ]);

        return $Lecture;
    }
}
