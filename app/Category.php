<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Category extends Eloquent
{
    //
    protected $table = 'xn_categories';


    protected $fillable =
    [
        // 'id',
        'title',
        'parent_id',
        
    ];

}


