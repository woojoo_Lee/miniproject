<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Lecture extends Eloquent
{
    //
    protected $table = 'xn_lectures';


    protected $fillable =
    [
        'category_id',
        'title',
        'teacher',
        'description',
        'lecture_image',
        
    ];

}
