<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
class LecturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('mongodb')->table('xn_lectures')->insert([
            'title' => Str::random(10),
            "teacher"=>Str::random(10),
            "description"=>Str::random(10),
            "lecture_image"=>Str::random(10),
            'category_id'=>Str::random(24)

           
        ]);
    }
}