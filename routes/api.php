<?php
namespace App\Http\Controllers\Api\v1;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// Route::get('/lectures',"LecturesController@index");




Route::prefix('v1')->group(function () {
      /**
     * category API
     */
    Route::get('categories','Api\v1\Category\CategoryApiController@index');
    Route::get('categories/{category_id}/lectures', 'Api\v1\Category\CategoryApiController@show');
    Route::post('categories', 'Api\v1\Category\CategoryApiController@create');
    Route::post('categories/{category_id}/delete', 'Api\v1\Category\CategoryApiController@destroy');

   /**
     * lecture API
     */

    Route::get('lectures','Api\v1\Lecture\LectureApiController@index');
    Route::get('lectures/{lectures_id}','Api\v1\Lecture\LectureApiController@show');
    Route::get('lectures/{image_url}/image','Api\v1\Lecture\LectureApiController@image');
    Route::post('lectures','Api\v1\Lecture\LectureApiController@create');
    Route::post('lectures/{lectures_id}/delete','Api\v1\Lecture\LectureApiController@destroy');

   
    
    
});



// Route::group(['prefix' => 'v1','as'=>'v1'], function () {
//     Route::get('/',"Lecture\LecturesController@index")->name('test');
// });